const state = {
    firstFighter: {},
    secondFighter: {},
    pressedKeyboardKeys: new Set(),
  };
  
  export const setInitialState = (...data) => {
    Object.entries(state)
      .filter(([_, value]) => !(value instanceof Set))
      .forEach(([key], index) => {     
        state[key].info = data[index];
        state[key].block = false;
        state[key].current_health = data[index].health;
        state[key].blockCriticalHit = false;
      });
  };
  
  export const toggleKeyboardKeys = (key, action) => {
    if (action == 'add') {
      state.pressedKeyboardKeys.add(key);
    } else {
      if (state.pressedKeyboardKeys.has(key)) {
        state.pressedKeyboardKeys.delete(key);
      }
    }
  };
  
  export const setStatusFighterBlock = (number, status) => {
    if (number == 'left') {
      state.firstFighter.block = status;
    } else {
      state.secondFighter.block = status;
    }
  };
  
  export const setStatusFighterCriticalHitBlock = (position, status) => {
    if (position == 'left') {
      state.firstFighter.blockCriticalHit = status;
    } else {
      state.secondFighter.blockCriticalHit = status;
    }
  };
  
  export const getStatusFightersCriticalHitBlock = () =>   
    [
      state.firstFighter.blockCriticalHit,
      state.secondFighter.blockCriticalHit
    ];
  
  export const getStatusFightersBlock = () =>   
    [
      state.firstFighter.block,
      state.secondFighter.block
    ];
  
  export const getFightersInfo = () => ({
    firstFighter: state.firstFighter,
    secondFighter: state.secondFighter,
  });
  
  export const setFighterHealth = (number, health) => {
    if (number == 'left') {
      state.firstFighter.current_health -= health;
    } else {
      state.secondFighter.current_health -= health;
    }
  };
  
  export const calcHealthIndicator = (fighter, position) => {
    const defenderInitHealth = fighter.health;
    const key = position == 'left' ? 'firstFighter' : 'secondFighter';
    const currentDefenderHealth = state[key].current_health;  
    return currentDefenderHealth / defenderInitHealth * 100;  
  };
  
  export const checkKeysCriticalHit = keys => {  
    for (const key of keys) {
      if (!state.pressedKeyboardKeys.has(key)) {
        return false;
      }
    }
    return true;
  };