import { controls } from '../../constants/controls';
import { fighterAction } from '../components/fight';
import { 
  toggleKeyboardKeys, 
  setStatusFighterBlock, 
  getStatusFightersBlock,
  getFightersInfo,
  checkKeysCriticalHit,
  getStatusFightersCriticalHitBlock
} from './state';

const {
  PlayerOneAttack, 
  PlayerOneBlock, 
  PlayerTwoAttack, 
  PlayerTwoBlock, 
  PlayerOneCriticalHitCombination, 
  PlayerTwoCriticalHitCombination
} = controls;

export const keyDownHandler = (fighters, resolve, event) => {
  let fighters_block_status;
  const { code } = event;
  toggleKeyboardKeys(code, 'add');   
  switch (code) {
    case PlayerOneAttack:      
      fighters_block_status = getStatusFightersBlock();
      if (fighters_block_status.filter(e => e).length) return;
      fighterAction(...fighters, 'right');
      break;    
    case PlayerTwoAttack:  
      fighters_block_status = getStatusFightersBlock();
      if (fighters_block_status.filter(e => e).length) return;
      fighterAction(...[...fighters].reverse(), 'left');
      break;
    case PlayerOneBlock:
      setStatusFighterBlock('left', true);
      break;
    case PlayerTwoBlock:      
      setStatusFighterBlock('right', true);
      break;
  }  
  const fighters_critical_hit_block_status = getStatusFightersCriticalHitBlock();
  if (!fighters_critical_hit_block_status[0] && 
    checkKeysCriticalHit(PlayerOneCriticalHitCombination)) {
      fighterAction(...fighters, 'right', true);
  } else if (!fighters_critical_hit_block_status[1] &&
    checkKeysCriticalHit(PlayerTwoCriticalHitCombination)) {
      fighterAction(...[...fighters].reverse(), 'left', true);
  }
  const { firstFighter, secondFighter } = getFightersInfo();
  if (firstFighter.current_health <= 0) {
    resolve(fighters[1]);
  } else if (secondFighter.current_health <= 0) {
    resolve(fighters[0]);
  }
};

export const keyUpHandler = event => {
  const { code } = event;
  toggleKeyboardKeys(code, 'delete');
  switch (code) {
    case PlayerOneBlock:
      setStatusFighterBlock('left', false);  
      break; 
    case PlayerTwoBlock:      
      setStatusFighterBlock('right', false);  
      break;
  }
};